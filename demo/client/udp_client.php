<?php
$client = new swoole_client(SWOOLE_SOCK_UDP);
$client->connect('127.0.0.1',9502);

// php cli
fwrite(STDOUT,'请输入信息:');
$msg = trim(fgets(STDIN));

// 发送消息给udp server 服务器
$client->send('发送:'.$msg);
$result = $client->recv();

echo "\n".$result."\n";

/*
$client->send('北京时间:' .date('Y-m-d H:i:s',time()));
$result = $client->recv();
echo "\n".$result."\n";
*/
