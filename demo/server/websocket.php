<?php
$ws = new swoole_websocket_server('0.0.0.0',9109);
$ws->set([
    'enable_static_handler' => true,
    'document_root' => '/home/centos/public_html/sw/demo/html'
]);
$ws->on('open','onOpen');
function onOpen($server,$request){
    echo "server: handshake success with fd{$request->fd}\n";
}

$ws->on('message',function(swoole_websocket_server $server,$frame){
    echo 'receive from'.$frame->fd.' : '.$frame->data.',opcode:'.$frame->opcode.',fin'.$frame->finish."\n";
    $server->push($frame->fd,'来自服务器的推送：hello,joy');
});

$ws->on('close',function($serve,$fd){
    echo 'client '.$fd.' closed'."\n";
});

$ws->start();
