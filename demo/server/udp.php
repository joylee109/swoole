<?php
// 创建server 对象，监听127.0.0.1:9501 端口
//  explain: https://wiki.swoole.com/wiki/page/14.html
$serv = new swoole_server('127.0.0.1',9502,SWOOLE_PROCESS,SWOOLE_SOCK_UDP);

$serv->set([
    'worker_num' => 8,
    'max_request' => 5000
]);

/*
$serv->on('connect',function($serv,$fd,$reactorId){
    echo 'Client:connect. - '.$reactorId.' - '.$fd;
});

$serv->on('receive',function($serv,$fd,$reactor_id,$data){
    echo 'server receive some data - '.$reactor_id.' - '.$fd."\n";
    var_dump($data);
    $serv->send($fd,' server receive some data - server:[ '.$data.' ]');
});


$serv->on('close',function($serv,$fd,$rector_id){
    echo 'Client:close  - '.$rector_id.' - '.$fd."\n";
});
*/
// 监听数据接收事件
$serv->on('Packet',function($serv,$data,$clientInfo){
    $serv->sendto($clientInfo['address'],$clientInfo['port'],'Server'.$data);
    var_dump($clientInfo);
});

// 启动服务器
$serv->start();