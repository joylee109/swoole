<?php
// 创建server 对象，监听127.0.0.1:9501 端口
//  explain: https://wiki.swoole.com/wiki/page/14.html
$serv = new swoole_server('127.0.0.1',9501);

// explain: https://wiki.swoole.com/wiki/page/13.html
$serv->set([
    'max_request' => 2000,
    'worker_num' => 8, // worker 进程数，cpu 1-4倍
]);
// 修改server配置项如worker_num、监听端口必须要重启Server。如果只改onReceive回调中的代码，可以使用reload实现热加载。[ https://group.swoole.com/question/106004 ]
// 监听连接进入事件
/**
 * $server是Swoole\Server对象
 * $fd是连接的文件描述符，发送数据/关闭连接时需要此参数
 * $reactorId来自哪个Reactor线程 线程id
 */
$serv->on('connect',function($serv,$fd,$reactorId){
     echo 'client:connect - '.$reactorId.' - '.$fd."\n";
});

// 监听数据接收事件
/**
 * $server，swoole_server对象
 * $fd，TCP客户端连接的唯一标识符
 * $reactor_id，TCP连接所在的Reactor线程ID
 * $data，收到的数据内容，可能是文本或者二进制内容
 */

$serv->on('receive',function($serv,$fd,$rector_id,$data){
    $serv->send($fd,'Server -'.$rector_id.' - '.$fd.' - '.$data);
});

// 监听连接关闭事件
$serv->on('close',function($serv,$fd,$rector_id){
    echo 'Client:close  - '.$rector_id.' - '.$fd."\n";
});

// 启动服务器
$serv->start();